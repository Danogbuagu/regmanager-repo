export class User {
  username: string;
  email: string;
  sname: string;
  fname: string;
  onames: string;
  phoneNo: string;
  roles: string[];
  visits: number;
  active: boolean;
  password: string;
  constructor() {}
}
