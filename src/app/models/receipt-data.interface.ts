export class ReceiptData {
  sname: string;
  fname: string;
  onames: string;
  phoneNo: string;
  loggedInUser: string;
  receiptDate: string;
  formNo: string;
  amount: number;
  jamb: number;
  svc_charge: number;
  group: string;
  constructor() {}
}
