export class BasicSpeechOptions {
  amplitude: number; // //How loud the voice will be (default: 100)
  pitch: number; // The voice pitch (default: 50) *
  speed: number; // The speed at which to talk (words per minute) (default: 175) //*
  voice: any; // Which voice to use (default: last voice loaded or defaultVoice, see below) *
  wordgap: number; // Additional gap between words in 10 ms units (default: 0) *
  variant: string; // One of the variants to be found in the eSpeak-directory

  // "~/espeak-data/voices/!v" Variants add some effects to the normally plain voice,
  // e.g. notably a female tone.
  // Valid values are: "f1", "f2", "f3", "f4", "f5" for female voices
  //  "m1", "m2", "m3", "m4", "m5", "m6, "m7" for male voices
  //  "croak", "klatt", "klatt2", "klatt3", "whisper", "whisperf" for other effects.
  //  (Using eSpeak, these would be appended to the "-v" option by "+" and the value.)
  //  Note: Try "f2" or "f5" for a female voice.
}
