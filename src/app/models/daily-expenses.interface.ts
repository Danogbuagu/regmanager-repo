export class DailyExpenses {
  expenseDate: Date;
  category: string;
  expenseDetails: string;
  amount: number;
  loggedInUser: string;
  approvedBy: string;
  constructor() {
    this.expenseDate = new Date();
  }
}
