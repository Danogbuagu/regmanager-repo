import { Component, NgZone } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { User } from './models/user';
import { AppPage } from '../../e2e/src/app.po';
import { UserService } from './services/user.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public loggedInUser: User = new User();
  public userRoles = new Array<string>();

  loggedIn = true;
  public appPages = [];
  public appPages$ = [
    // {
    //   title: 'Home',
    //   url: '/home',
    //   icon: 'home'
    // },
    // {
    //   title: 'Login',
    //   url: '/login',
    //   icon: 'log-in'
    // },
    {
      title: 'Logout',
      url: '/login',
      icon: 'log-out'
    },
    {
      title: '--- GENERAL OPERATIONS ---',
      url: '',
      icon: ''
    },
    {
      title: 'Print Queue Numbers',
      url: '/print-qnos',
      icon: 'speedometer'
    },
    {
      title: 'Call Queue Numbers',
      url: '/call-qnos',
      icon: 'volume-high'
    },
    {
      title: 'Add Candidate',
      url: '/add-person',
      icon: 'school'
    },
    {
      title: 'Sell Form',
      url: '/sell-form',
      icon: 'thumbs-up'
    },
    {
      title: '--- ACCOUNTS & REPORTS ---',
      url: ' ',
      icon: ''
    },
    {
      title: 'Enter Expenses',
      url: '/daily-expenses',
      icon: 'cash'
    },
    {
      title: 'Sales List',
      url: '/sales-list',
      icon: 'paper'
    },
    {
      title: 'Daily Sales Report',
      url: '/sales-report',
      icon: 'business'
    },
    {
      title: 'Daily Expenses Report',
      url: '/expense-report',
      icon: 'star'
    },
    {
      title: 'Daily General Report',
      url: '/sales-rpt-general',
      icon: 'stats'
    },
    {
      title: 'PIN Vending Records',
      url: '/pin-vending',
      icon: 'recording'
    },
    {
      title: '--- ADMIN SECTION ---',
      url: '',
      icon: ''
    },
    {
      title: 'Add Client Group',
      url: '/add-bizgroup',
      icon: 'add-circle'
    },
    {
      title: 'Generate Queue Numbers',
      url: '/gen-qnos',
      icon: 'repeat'
    },
    {
      title: 'Register User',
      url: '/user',
      icon: 'person-add'
    },
    {
      title: 'Users',
      url: '/userlist',
      icon: 'contacts'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private authSvc: UserService,
    private zone: NgZone,
    private router: Router,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
    this.showForCeo();
  //  this.setupWithoutLogin();
   // this.loggedIn = !!localStorage.getItem('auth_token');
  }

  async setupWithoutLogin() {
    // const retrieved = localStorage.getItem('auth_token');
    // console.log(
    //   `app.component.ts: 132 => Token obtained from storage => ${JSON.stringify(
    //     retrieved
    //   )}`
    // );

    // const tokenParts = retrieved.split('.');
    // const encodedPayload = tokenParts[1];
    //  console.log(`Encoded payload => ${encodedPayload}`);
  //  const rawPayload = atob(encodedPayload);
    //  console.log(`Raw payload => ${rawPayload}`);
  //  const user = JSON.parse(rawPayload);
    // console.log(`User in payload => ${JSON.stringify(user)}`);

  //  const uname = user.username;
    //  console.log(`Username in payload => ${uname}`);

    // let arr: User[];

    // const allUsers: Observable<Object> = await this.authSvc.getUsers();

    // await allUsers.subscribe(usr => {
    //   // console.log(`All users: ${JSON.stringify(usr)}`);
    //   this.zone.run(() => {
    //     arr = usr as User[];
    //     arr.forEach(it => {
    //      // console.log(`User in array:${it.sname}, ${it.fname} ${it.onames} `);
    //     });
    //     this.loggedInUser = arr.find(_ => _.username === uname);
    //     // console.log(
    //     //   `User found: ${this.loggedInUser.sname}, ${this.loggedInUser.fname} ${
    //     //     this.loggedInUser.onames
    //     //   } `
    //     // );
    //     if (this.loggedInUser) {
    //       this.userRoles = this.loggedInUser.roles;
    //     //  console.log(`Roles of User found: ${this.loggedInUser.roles} `);
    //     }
    //   });
    // });
  }

  // async setupWithLogin() {
  //   await this.router.navigateByUrl('/login');
  //   await this.showForUser();
  //   const retrieved = localStorage.getItem('auth_token');
  //   // console.log(
  //   //   `app.component.ts: 132 => Token obtained from storage => ${JSON.stringify(
  //   //     retrieved
  //   //   )}`
  //   // );

  //   const tokenParts = retrieved.split('.');
  //   const encodedPayload = tokenParts[1];
  //   //  console.log(`Encoded payload => ${encodedPayload}`);
  //   const rawPayload = atob(encodedPayload);
  //   //  console.log(`Raw payload => ${rawPayload}`);
  //   const user = JSON.parse(rawPayload);
  //   // console.log(`User in payload => ${JSON.stringify(user)}`);

  //   const uname = user.username;
  //   //  console.log(`Username in payload => ${uname}`);

  //   let arr: User[];

  //   const allUsers: Observable<Object> = await this.authSvc.getUsers();

  //   await allUsers.subscribe(usr => {
  //     //  console.log(`All users: ${JSON.stringify(usr)}`);
  //     this.zone.run(() => {
  //       arr = usr as User[];
  //       // arr.forEach(it => {
  //       //   console.log(`User in array:${it.sname}, ${it.fname} ${it.onames} `);
  //       // });
  //       this.loggedInUser = arr.find(_ => _.username === uname);
  //       // console.log(
  //       //   `User found: ${this.loggedInUser.sname}, ${this.loggedInUser.fname} ${
  //       //     this.loggedInUser.onames
  //       //   } `
  //       // );
  //       if (this.loggedInUser) {
  //         this.userRoles = this.loggedInUser.roles;
  //         //  console.log(`Roles of User found: ${this.loggedInUser.roles} `);
  //         this.setupMenu(this.userRoles);
  //       }
  //     });
  //   });
//  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  setupMenu(rolesArray: string[]) {
    // find contents of roles

    if (rolesArray === ['user']) {
      console.log(`Roles includes: user`);
      this.showForUser();
      // this.initializeApp();
    } else if (rolesArray.includes('accounts')) {
      console.log(`Roles includes: accounts`);
      this.showForAccounts();
      // this.initializeApp();
    } else if (rolesArray.includes('management')) {
      console.log(`Roles includes: management`);
      this.showForManagement();
      // this.initializeApp();
    } else if (rolesArray.includes('admin')) {
      console.log(`Roles includes: admin`);
      this.showForAdmin();
      // this.initializeApp();
    } else if (rolesArray.includes('ceo')) {
      console.log(`Roles includes: ceo`);
      this.showForCeo();
      // this.initializeApp();
    } else {
      this.showForUser();
      // this.initializeApp();
    }
  }

  private showForUser() {
    this.appPages = [];
    this.appPages = [
      {
        title: 'Home',
        url: '/home',
        icon: 'home'
      },
      {
        title: 'Logout',
        url: '/login',
        icon: 'log-out'
      },
      {
        title: '--- GENERAL OPERATIONS ---',
        url: '',
        icon: ''
      },
      {
        title: 'Print Queue Numbers',
        url: '/print-qnos',
        icon: 'speedometer'
      },
      {
        title: 'Call Queue Numbers',
        url: '/call-qnos',
        icon: 'volume-high'
      },
      {
        title: 'Add Candidate',
        url: '/add-person',
        icon: 'school'
      },
      {
        title: 'Sell Form',
        url: '/sell-form',
        icon: 'thumbs-up'
      }
    ];
  }

  private showForAccounts() {
    this.appPages = [];
    this.appPages = [
      {
        title: 'Home',
        url: '/home',
        icon: 'home'
      },
      {
        title: 'Logout',
        url: '/login',
        icon: 'log-out'
      },
      {
        title: '--- ACCOUNTS & REPORTS ---',
        url: ' ',
        icon: ''
      },
      {
        title: 'Enter Expenses',
        url: '/daily-expenses',
        icon: 'cash'
      },
      {
        title: 'Sales List',
        url: '/sales-list',
        icon: 'paper'
      },
      {
        title: 'Daily Sales Report',
        url: '/sales-report',
        icon: 'business'
      },
      {
        title: 'Daily Expenses Report',
        url: '/expense-report',
        icon: 'star'
      },
      {
        title: 'Daily General Report',
        url: '/sales-rpt-general',
        icon: 'stats'
      },
      {
        title: 'PIN Vending Records',
        url: '/pin-vending',
        icon: 'recording'
      }
    ];
  }
  private showForManagement() {
    this.appPages = [];
    this.appPages = [
      {
        title: 'Home',
        url: '/home',
        icon: 'home'
      },
      {
        title: 'Logout',
        url: '/login',
        icon: 'log-out'
      },
      {
        title: '--- REPORTS ---',
        url: ' ',
        icon: ''
      },
      {
        title: 'Enter Expenses',
        url: '/daily-expenses',
        icon: 'cash'
      },
      {
        title: 'Sales List',
        url: '/sales-list',
        icon: 'paper'
      },
      {
        title: 'Daily Sales Report',
        url: '/sales-report',
        icon: 'business'
      },
      {
        title: 'Daily General Report',
        url: '/sales-rpt-general',
        icon: 'stats'
      },
      {
        title: 'PIN Vending Records',
        url: '/pin-vending',
        icon: 'recording'
      }
    ];
  }

  private showForAdmin() {
    this.appPages = [];
    this.appPages = [
      {
        title: 'Home',
        url: '/home',
        icon: 'home'
      },
      {
        title: 'Logout',
        url: '/login',
        icon: 'log-out'
      },
      {
        title: '--- ADMIN SECTION ---',
        url: '',
        icon: ''
      },
      {
        title: 'Add Client Group',
        url: '/add-bizgroup',
        icon: 'add-circle'
      },
      {
        title: 'Generate Queue Numbers',
        url: '/gen-qnos',
        icon: 'repeat'
      },
      {
        title: 'Register User',
        url: '/user',
        icon: 'person-add'
      },
      {
        title: 'Users',
        url: '/userlist',
        icon: 'contacts'
      }
    ];
  }

  private showForCeo() {
    this.appPages = [];
    this.appPages = this.appPages$;
  }

  setLoggedInUser() {}
}
