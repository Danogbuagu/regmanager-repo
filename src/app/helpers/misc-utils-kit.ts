import { Injectable } from '@angular/core';
import { ceil } from 'lodash-es';

@Injectable()
export class MiscUtilsKit {
  constructor() {}

  refreshPageByForce() {
    window.location.reload(true);
  }

  // Number to money format
  numberToMoneyStr(num: number): string {
    return new Intl.NumberFormat().format(ceil(num, 2));
  }

  /**
   * Set Input HTML Number element to be 2 places decimal float
   *
   * @param {any} el the HTML <input type="number"> element
   *
   * @memberOf MiscUtilsKit
   */
  setTwoNumberDecimal(el: any) {
    el.value = parseFloat(el.value).toFixed(2);
  }

  // sleep for some milliseconds
  sleep(milliseconds) {
    const start = new Date().getTime();
    for (let i = 0; i < 1e7; i++) {
      if (new Date().getTime() - start > milliseconds) {
        break;
      }
    }
  }

  public genIdFromTitle(title: string): string {
    return title
      .trim()
      .toLowerCase()
      .replace(/ /g, '-')
      .replace(/[^\w-]+/g, '');
  }

  public rootNameFromEmail(eml: string): string {
    const arr = eml.split('@');
    return arr[0].trim().toUpperCase();
  }

  /**
   * Get month name as Jan Feb, etc., based on current date
   *
   * @returns {string}  Month names like 'Jan', 'Feb', etc.
   *
   * @memberOf MiscUtilsKit
   */
  getMonthName(): string {
    const n: number = parseInt(this.getTodayMoStr(), 10);
    // console.log('Calculated month no: ' + n);
    let retStr: string;
    switch (n) {
      case 1:
        retStr = 'Jan';
        break;
      case 2:
        retStr = 'Feb';
        break;
      case 3:
        retStr = 'Mar';
        break;
      case 4:
        retStr = 'Apr';
        break;
      case 5:
        retStr = 'May';
        break;
      case 6:
        retStr = 'Jun';
        break;
      case 7:
        retStr = 'Jul';
        break;
      case 8:
        retStr = 'Aug';
        break;
      case 9:
        retStr = 'Sep';
        break;
      case 10:
        retStr = 'Oct';
        break;
      case 11:
        retStr = 'Nov';
        break;
      case 12:
        retStr = 'Dec';
        break;
      default:
        retStr = 'invalid month';
    }
    return retStr;
  }

  /**
   * Accepts month values like numeric values quoted as strings,
   * and returns corresponding month names.
   *
   * @param {string} moStr String representing month in the form '01', '02',... '12' etc.
   * @returns {string} Month names like 'Jan', 'Feb', etc.
   *
   * @memberOf DateStringsKit
   */
  getMonthNameFrom(moStr: string): string {
    let retStr: string;
    switch (moStr) {
      case '01':
        retStr = 'Jan';
        break;
      case '02':
        retStr = 'Feb';
        break;
      case '03':
        retStr = 'Mar';
        break;
      case '04':
        retStr = 'Apr';
        break;
      case '05':
        retStr = 'May';
        break;
      case '06':
        retStr = 'Jun';
        break;
      case '07':
        retStr = 'Jul';
        break;
      case '08':
        retStr = 'Aug';
        break;
      case '09':
        retStr = 'Sep';
        break;
      case '10':
        retStr = 'Oct';
        break;
      case '11':
        retStr = 'Nov';
        break;
      case '12':
        retStr = 'Dec';
        break;
      default:
        retStr = 'invalid month';
    }
    return retStr;
  }

  /**
   * Get string representation of current year
   *
   * @returns {string} string representation of current year, e.g. '2017'.
   *
   * @memberOf DateStringsKit
   */
  getTodayYrStr(): string {
    const date = new Date();
    const todayStr: string = date.toISOString().slice(0, 10); // This will output the date in YYYY-MM-DD format:
    const yrStr: string = todayStr.slice(0, 4);
    return yrStr;
  }

  getNowAsString(): string {
    const date = new Date();

    // This will output the date in YYYY-MM-DD format:
    const todayStr: string = date.toISOString().slice(0, 10);

    return todayStr;
  }

  /**
   * Get string representation of current month as string-quoted digits
   *
   * @returns {string} string representing month in the form '01', '02',... '12' etc.
   *
   * @memberOf DateStringsKit
   */
  getTodayMoStr(): string {
    const date = new Date();
    const todayStr: string = date.toISOString().slice(0, 10); // This will output the date in YYYY-MM-DD format:
    const moStr: string = todayStr.slice(5, 7);
    return moStr;
  }

  /**
   * Get string representation of current year, and month as string-quoted digits
   *
   * @returns {string} string representation of current year and month, e.g. '2017_06'.
   *
   * @memberOf DateStringsKit
   */
  getTodayYrMoStr(): string {
    return this.getTodayYrStr() + '_' + this.getTodayMoStr();
  }

  /**
   * Return current month and year, in a user-friendly format, e.g. 'Jan 2017'
   *
   * @returns {string}
   *
   * @memberOf DateStringsKit
   */
  getTodayYrMoStrWithName(): string {
    return this.getMonthName() + ' ' + this.getTodayYrStr();
  }

  /**
   * Get today's day number - day in the current month
   *
   * @returns {string} returns day number of today in the current month.
   *
   * @memberOf DateStringsKit
   */
  getTodayDayStr(): string {
    const date = new Date();
    const todayStr: string = date.toISOString().slice(0, 10); // This will output the date in YYYY-MM-DD format:
    const dayStr: string = todayStr.slice(8, 10);
    return dayStr;
  }

  isoToNigerianDate(isoDate: string): string {
    const d = isoDate.slice(8, 10);
    const m = this.getMonthNameFrom(isoDate.slice(5, 7));
    const y = isoDate.slice(0, 4);

    return d + ' ' + m + ' ' + y;
  }

  isoToDateObject(isoDateStr: string): Date {
    let isoStr = JSON.stringify(isoDateStr);
    isoStr = JSON.parse(isoStr);
    const y = isoStr['year'].value;
    const m = isoStr['month'].value;
    const d = isoStr['day'].value;

    const date = new Date(parseInt(y, 10), parseInt(m, 10), parseInt(d, 10));
    const yy = date.getFullYear();
    const mo = date.getMonth();
    const dd = date.getDate();
    return new Date(yy, mo, dd);
  }
}
