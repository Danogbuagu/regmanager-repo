export class Promisify {
  constructor() {}

  functionReturningPromise(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      setTimeout(() => {
        console.log('Async Work Complete');
        resolve();
        reject((err: Error) => {
          console.log(`Promise failed due to ${err.message}`);
        });
      }, 1000);
    });
  }
}
