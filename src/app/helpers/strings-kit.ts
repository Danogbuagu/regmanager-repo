import { Injectable } from '@angular/core';

@Injectable()
export class StringsKit {
    constructor() { }

    // Title Case
    toTitleCase(sentence: string): string {
        if (sentence === null || sentence === '' || sentence === undefined) {
            return sentence;
        }
        return sentence.toLowerCase().split(' ').map((word) => {
            return word.replace(word[0], word[0].toUpperCase());
        }).join(' ');
    }

    replaceSlashWithDash(sourceStr: string): string {
        return sourceStr.replace(/\//g, '-');
    }


    replaceSlashWithOr(sourceStr: string): string {
        return sourceStr.replace(/\//g, ' OR ');
    }

    replaceCommaWithUnderscore(sourceStr: string): string {
        return sourceStr.replace(/\,/g, '_');
    }

    replaceDotWithUnderscore(sourceStr: string): string {
        return sourceStr.replace(/\./g, '_');
    }

    removeDot(sourceStr: string): string {
        return sourceStr.replace(/\./g, '');
    }
    replaceSlashWithUnderscore(sourceStr: string): string {
        return sourceStr.replace(/\//g, '_');
    }

    replaceAmpersandWithUnderscore(sourceStr: string): string {
        return sourceStr.replace(/\&/g, '_');
    }

    replaceSpaceWithUnderscore(sourceStr: string): string {
        let result = '';
        const strs = sourceStr.split(' ');
        if (strs.length > 1) {
            result = strs.join('_');
        } else {
            result = sourceStr;
        }

        return result;
    }

    replaceCommaWithHyphen(sourceStr: string): string {
        return sourceStr.replace(/\,/g, '-');
    }

}
