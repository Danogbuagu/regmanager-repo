import { Injectable } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';

@Injectable()
export class AlertUtils {
  constructor(public alertController: AlertController, private toastCtl: ToastController) {}

  async presentAlert(header: string, subHeader: string, msg: string) {
    const alert = await this.alertController.create({
      header: header,
      subHeader: subHeader,
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentToast(msg: string, duration: number) {
    const toast  = await this.toastCtl.create({
      message: msg,
      duration: duration
    });
    await toast.present();
  }

  async presentAlertMultipleButtons(
    header: string,
    subHeader: string,
    msg: string
  ) {
    const alert = await this.alertController.create({
      header: header,
      subHeader: subHeader,
      message: msg,
      buttons: ['Cancel', 'Open', 'Delete']
    });

    await alert.present();
  }

  async presentAlertConfirm(msg: string) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: msg,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {
            console.log('Confirm Cancel: ' + blah);
          }
        },
        {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
}
