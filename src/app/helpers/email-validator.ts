import { FormControl } from '@angular/forms';
import { Injectable } from '@angular/core';

@Injectable()
export class EmailValidator {

  static isValid(control: FormControl) {

    // tslint:disable-next-line:max-line-length
    const re = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/.test(control.value);

    if (re) {
      return null;
    }

    return { 'invalidEmail': true };
  }

}
