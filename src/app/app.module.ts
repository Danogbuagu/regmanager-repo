import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MiscUtilsKit } from './helpers/misc-utils-kit';
import { StringsKit } from './helpers/strings-kit';
import { AgeValidator } from './helpers/age-validator';
import { AlertUtils } from './helpers/alert-utils';
import { EmailValidator } from './helpers/email-validator';
import { AuthGuard } from './auth/auth.guard';

export function jwtOptionsFactory(storage) {
  return {
    tokenGetter: () => {
      return storage.get('access_token');
    }
  };
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    MiscUtilsKit,
    StringsKit,
    EmailValidator,
    AgeValidator,
    AlertUtils,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
