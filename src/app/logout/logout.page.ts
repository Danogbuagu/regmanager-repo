import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss']
})
export class LogoutPage implements OnInit {
  constructor(private authSvc: UserService, private router: Router) {
    this.router.navigate(['/login']);
  }

  ngOnInit() {
    this.authSvc.signOut();
    this.router.navigateByUrl('/login');
  }
}
