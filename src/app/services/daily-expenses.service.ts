import { Injectable } from '@angular/core';
import { StringsKit } from '../helpers/strings-kit';
import {
  HttpHeaders,
  HttpClient,
  HttpErrorResponse
} from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { DailyExpenses } from '../models/daily-expenses.interface';

@Injectable({
  providedIn: 'root'
})
export class DailyExpensesService {
  constructor(private http: HttpClient) {}


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer {JWT Token}'
    })
  };

  endpointUrl = 'http://localhost:9900/daily-expenses';

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a  dlyExp-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  getDailyExpenses(): Observable<Object> {
    return this.http.get(this.endpointUrl);
  }

  /** POST: add a new  dlyExp to the database */
  createNew(dlyExps: DailyExpenses): Observable<DailyExpenses> {
    console.log(
      ` DailyExpenses object arriving local service: ${JSON.stringify(dlyExps)}`
    );

    const ret = this.http.post<DailyExpenses>(
      this.endpointUrl,
      dlyExps,
      this.httpOptions
    );
    ret.subscribe(e => {
      console.log(
        `Maybe saved records for ${e.category}, ${e.expenseDetails} ${
          e.amount
        } to database.`
      );
    });
    return ret;
  }

  getExpensesOnDate(expDate: string): any {
    console.log(
      `...... get DailyExpenses() accessed with  DailyExpenses ID of ${expDate} ..........`
    );
    const newUrl = this.endpointUrl + '/' + expDate;
    return this.http.get(newUrl);
  }
}
