import { TestBed } from '@angular/core/testing';

import { SalesRptPrintService } from './sales-rpt-print.service';

describe('SalesRptPrintService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SalesRptPrintService = TestBed.get(SalesRptPrintService);
    expect(service).toBeTruthy();
  });
});
