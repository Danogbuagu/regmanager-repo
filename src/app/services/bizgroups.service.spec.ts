import { TestBed } from '@angular/core/testing';

import { BizgroupsService } from './bizgroups.service';

describe('BizgroupsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BizgroupsService = TestBed.get(BizgroupsService);
    expect(service).toBeTruthy();
  });
});
