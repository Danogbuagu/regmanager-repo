import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
  HttpParams
} from '@angular/common/http';
import { StringsKit } from '../helpers/strings-kit';
import { throwError, Observable } from 'rxjs';
import * as pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { ReceiptData } from '../models/receipt-data.interface';
import { MiscUtilsKit } from '../helpers/misc-utils-kit';

@Injectable({
  providedIn: 'root'
})
export class ReceiptsService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer {JWT Token}'
    })
  };

  endpointUrl = 'http://localhost:9900/receipts';

  constructor(
    private http: HttpClient,
    private miscKit: MiscUtilsKit,
    private strkit: StringsKit
  ) {}

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a receipt-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  createDocDefinition(receiptData: ReceiptData, price: number): any {
    pdfMake.fonts = {
      Roboto: {
        normal: 'Roboto-Regular.ttf',
        bold: 'Roboto-Medium.ttf',
        italics: 'Roboto-Italic.ttf',
        bolditalics: 'Roboto-MediumItalic.ttf'
      }
    };

    const fullName =
      receiptData.sname + ', ' + receiptData.fname + ' ' + receiptData.onames;
    console.log(`Preparing Receipt for ${fullName}`);

    const dd = {
      // a string or { width: number, height: number }
      // pageSize: 'B8',
      pageSize: { width: 200, height: 240 },

      // by default we use portrait, you can change it to landscape if you wish
      pageOrientation: 'portrait',

      // [left, top, right, bottom] or [horizontal, vertical] or just a number for equal margins
      pageMargins: [8, 2, 2, 4],
      content: [
        { text: 'Swintec Computer Institute,', style: 'header' },
        {
          text: 'JAMB ACCREDITED REG/EXAM CENTRE',
          style: 'subheader',
          alignment: 'center'
        },
        { text: '5/9 Hospital/Orlu Rd., Owerri.', alignment: 'center' },
        { text: '(Call: 0802-975-4376 OR 0806-362-0194)', alignment: 'center' },

        {
          text: 'Registration Form Receipt',
          style: 'subheader',
          margin: [0, 12],
          alignment: 'center'
        },
        { text: 'Date: ' + receiptData.receiptDate },
        { text: 'Receipt No: ' + receiptData.formNo },
        { text: 'Form No: ' + receiptData.formNo },

        { text: 'Received From: ' + fullName, style: 'subheader' },
        { text: 'Phone: ' + receiptData.phoneNo + '.' },
        { text: 'Payment for JAMB ' },

        {
          text:
            'Amount Paid: N' +
            this.miscKit.numberToMoneyStr(Number(receiptData.amount).valueOf()),
          style: 'subheader',
          margin: [0, 12]
        },

        {
          text: 'Received by: ' + receiptData.loggedInUser + '.',
          style: 'subheader'
        }
      ],
      styles: {
        header: {
          fontSize: 10,
          bold: true,
          margin: [0, 0, 0, 1],
          alignment: 'center'
        },
        subheader: {
          fontSize: 8,
          bold: true,
          margin: [0, 1, 0, 1]
        }
      },
      defaultStyle: {
        fontSize: 8
      }
    };

    return dd;
  }

  getReceipts(): Observable<Object> {
    return this.http.get(this.endpointUrl);
  }

  saveReceipt(receipt: ReceiptData): Observable<ReceiptData> {
    receipt.fname = this.strkit.toTitleCase(receipt.fname);
    receipt.onames = this.strkit.toTitleCase(receipt.onames);
    receipt.sname = receipt.sname.toUpperCase();

    // console.log(
    //   `ReceiptData object arriving local service: ${JSON.stringify(receipt)}`
    // );

    const ret = this.http.post<ReceiptData>(
      this.endpointUrl,
      receipt,
      this.httpOptions
    );
    ret.subscribe(e => {
      console.log(
        `Maybe saved records for ${e.sname}, ${e.fname} ${
          e.onames
        } to database.`
      );
    });
    return ret;
  }

  getReceiptsOnDate(rptDate: string): Observable<Object> {
    console.log(
      `Date as string entering getReceiptsOnDate() in receipts service is => ${rptDate}`
    );
    const newUrl = this.endpointUrl + '/' + rptDate;

    return this.http.get(newUrl);
  }
}
