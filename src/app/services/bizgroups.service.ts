import { Injectable } from '@angular/core';
import {
  HttpHeaders,
  HttpClient,
  HttpErrorResponse
} from '@angular/common/http';
import { StringsKit } from '../helpers/strings-kit';
import { throwError, Observable } from 'rxjs';
import { Bizgroup } from '../models/bigroup.interface';
import { Price } from '../models/price.interface';

@Injectable({
  providedIn: 'root'
})
export class BizgroupsService {
  constructor(private http: HttpClient, private strkit: StringsKit) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer {JWT Token}'
    })
  };

  endpointUrl = 'http://localhost:9900/bizgroups';

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a bizgroup-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  getBizgroups(): Observable<Object> {
    return this.http.get(this.endpointUrl);
  }

  /** POST: add a new bizgroup to the database */
  createNew(bizgroup: Bizgroup): Observable<Bizgroup> {
    console.log(
      `Bizgroup object arriving local service: ${JSON.stringify(bizgroup)}`
    );

    const ret = this.http.post<Bizgroup>(
      this.endpointUrl,
      bizgroup,
      this.httpOptions
    );
    ret.subscribe(e => {
      console.log(
        `Maybe saved records for ${e.groupname}, ${e.formAmount} ${
          e.serviceCharge
        } to database.`
      );
    });
    return ret;
  }

  // getPrices(): Price[] {
  //   const ret = new Array<Price>();
  //   const groups: Observable<Object> = this.getBizgroups();
  //   groups.subscribe(grps => {
  //     const grpObjs = grps as Bizgroup[];
  //     grpObjs.forEach(_ => {
  //       ret.push({ formAmt: _.formAmount, svcChrge: _.serviceCharge });
  //     });
  //   });
  //   return ret;
  // }
}
