import { Injectable, NgZone } from '@angular/core';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { StringsKit } from '../helpers/strings-kit';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  //  user: User;
  private loggedIn = false;

  constructor(
    private http: HttpClient,
    private strkit: StringsKit,
    private zone: NgZone
  ) {
    this.loggedIn = !!localStorage.getItem('auth_token');
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer {JWT Token}'
    })
  };

  endpointUrl = 'http://localhost:9900/users';

  isloggedIn(): boolean {
    return this.loggedIn;
  }

  getUsers(): Observable<Object> {
    return this.http.get('http://localhost:9900/users');
  }

  /** POST: add a new user to the database */
  createNew(user: User): Observable<User> {
    user.username = user.username.toLowerCase();
    user.sname = user.sname.toUpperCase();
    user.fname = this.strkit.toTitleCase(user.fname);
    user.onames = this.strkit.toTitleCase(user.onames);

    const ret = this.http.post<User>(this.endpointUrl, user, this.httpOptions);
    ret.subscribe(e => {
      console.log(
        `Maybe saved records for ${e.sname}, ${e.fname} ${
        e.onames
        } to database.`
      );
    });
    return ret;
  }

  getUser(userid: string): Observable<Object> {
    console.log(
      `...... getUser() accessed with User ID of ${userid} ..........`
    );
    return this.http.get('http://localhost:9900/users/' + userid);
  }

  signInWithEmailAndPassword(
    email: string,
    password: string
  ): Observable<Object> {
    const authUrl = 'http://localhost:9900/auth/login';
    const payloadObj = { email, password };
    const result = this.http.post(
      authUrl,
      JSON.stringify(payloadObj),
      this.httpOptions
    );
    if (result) {
      console.log(
        `Upon successful Login, we get this JSON string token ${JSON.stringify(
          result
        )}`
      );
      localStorage.setItem('auth_token', JSON.stringify(result));
      this.loggedIn = true;
    } else {
      return null;
    }
    return result;
  }

  signInWithUsernameAndPassword(
    username: string,
    password: string
  ): Observable<Object> {
    const authUrl = 'http://localhost:9900/auth/login';
    const payloadObj = { username, password };
    const result = this.http.post(
      authUrl,
      JSON.stringify(payloadObj),
      this.httpOptions
    );
    if (result) {
    } else {
      return null;
    }
    return result;
  }

  authState(): Promise<Object> {
    const retval: Promise<Object> = null;

    localStorage
      .getItem('auth_state');
    return retval;
  }

  signOut(): any {
    localStorage.clear();
  }

  usernameFromPayload(accessTokenFromRESTServerAPI: string): string {

    const tokenParts = accessTokenFromRESTServerAPI.split('.');
    const encodedPayload = tokenParts[1];
    const rawPayload = atob(encodedPayload);
    const user = JSON.parse(rawPayload);

    return user.username;
  }

  saveAuthState(payloadJSON: string) {
    console.log(`Input payloadJSON Argument to saveAuthState() is:
    ${JSON.stringify(payloadJSON)}`);
    console.log(`Attempting to store result of Auth State to Local Storage`);
    localStorage.clear();
    console.log(`Cleared Local Storage ...`);

    localStorage
      .setItem('auth_state', payloadJSON);
  }

  getUserByUsername(username: string): User {
    console.log(
      `...... getUserByUsername() accessed with Username of ${username} ..........`
    );

    let user = new User();

    let arr: User[];

    const allUsers: Observable<Object> = this.getUsers();

    allUsers.subscribe(usr => {
      console.log(`All users: ${JSON.stringify(usr)}`);
      this.zone.run(() => {
        arr = usr as User[];
        // arr.forEach(it => {
        //   console.log(`User in array:${it.sname}, ${it.fname} ${it.onames} `);
        // });
        user = arr.find(_ => _.username === username);
        console.log(
          `User found: ${user.sname}, ${user.fname} ${
          user.onames
          } `
        );
        if (user) { return user; }
      });
      if (user) { return user; }
    });
    return user;
  }

  saveCurrentUserLocal(user: User) {
    localStorage
      .setItem('curr_user', user.username);
  }

  retrieveCurrUserLocal(): User {
    let retval = new User();
    const val = localStorage.getItem('curr_user');
    console.log(
      `Username: ${val}`);
    retval = this.getUserByUsername(val);

    return retval;
  }
}
