import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { StringsKit } from '../helpers/strings-kit';
import { throwError, Observable } from 'rxjs';
import { Quenumber } from '../models/quenumber';

import * as pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { MiscUtilsKit } from '../helpers/misc-utils-kit';
import { Prtnumber } from '../models/prtnumber';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root'
})
export class QuenosService {
  private $UID = 'currentUser';

  constructor(private http: HttpClient, private miscKit: MiscUtilsKit) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer {JWT Token}'
    })
  };

  endpointUrlQue = 'http://localhost:9900/quenumbers';
  endpointUrlPrt = 'http://localhost:9900/prtnumbers';

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a queno-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  getQuenumbers(): Observable<Object> {
    return this.http.get(this.endpointUrlQue);
  }

  getPrtnumbers(): Observable<Object> {
    return this.http.get(this.endpointUrlPrt);
  }

  /** POST: add a new queno to the database */
  createNew(queno: Quenumber): Observable<Quenumber> {
    //  console.log(`Quenumber object arriving local service: ${JSON.stringify(queno)}`);

    const ret = this.http.post<Quenumber>(
      this.endpointUrlQue,
      queno,
      this.httpOptions
    );
    ret.subscribe(e => {
      console.log(`Maybe saved records for ${e.queno} to database.`);
    });
    return ret;
  }

  getQuenumber(quenoid: string): Observable<Object> {
    console.log(
      `...... getQuenumber() accessed with Quenumber ID of ${quenoid} ..........`
    );
    return this.http.get(this.endpointUrlQue + '/' + quenoid);
  }

  deleteQueno(id: string): Observable<Quenumber> {
    const ret = this.http.delete<Quenumber>(this.endpointUrlQue + '/' + id);
    ret.subscribe(e => {
      console.log(
        `Maybe deleted Quenumber record for ${e.queno} from database.`
      );
    });
    return ret;
  }

  deletePrtno(id: string): Observable<Prtnumber> {
    const ret = this.http.delete<Prtnumber>(this.endpointUrlPrt + '/' + id);
    ret.subscribe(e => {
      console.log(
        `Maybe deleted Prtnumber record for ${e.queno} from database.`
      );
    });
    return ret;
  }

  //////// GENERATING NOS /////

  createPrtnum(prtno: Prtnumber): Observable<Prtnumber> {
    const ret = this.http.post<Prtnumber>(
      this.endpointUrlPrt,
      prtno,
      this.httpOptions
    );
    ret.subscribe(e => {
      // console.log(`Maybe saved records for ${e.sname}, ${e.fname} ${e.onames} to database.`);
    });
    return ret;
  }

  async generateNumbers(from: number, to: number) {
    let txt: string;

    for (let i = Number(from).valueOf(); i <= Number(to).valueOf(); i++) {
      txt = this.genNoStr(i);

      const qn = new Quenumber();
      qn.queno = parseInt(txt, 10);
      await this.createNew(qn);

      const pn = new Prtnumber();
      pn.queno = parseInt(txt, 10);
      await this.createPrtnum(pn);
    }
  }

  genNoStr(n: Number): string {
    let str = '';

    if (1 <= n && n < 10) {
      str = '0000' + n.toString();
    } else {
      if (10 <= n && n < 100) {
        str = '000' + n.toString();
      } else {
        if (100 <= n && n < 1000) {
          str = '00' + n.toString();
        } else {
          if (1000 <= n && n < 10000) {
            str = '0' + n.toString();
          } else {
            if (10000 <= n && n < 100000) {
              str = n.toString();
            }
          }
        }
      }
    }

    return str;
  }

  //////////////////// OTHERS //////

  createDocDefinition(queueNo: string): any {
    pdfMake.fonts = {
      Roboto: {
        normal: 'Roboto-Regular.ttf',
        bold: 'Roboto-Medium.ttf',
        italics: 'Roboto-Italic.ttf',
        bolditalics: 'Roboto-MediumItalic.ttf'
      }
    };
    const dd = {
      // a string or { width: number, height: number }
      // pageSize: 'B8',
      pageSize: { width: 200, height: 240 },

      // by default we use portrait, you can change it to landscape if you wish
      pageOrientation: 'portrait',

      // [left, top, right, bottom] or [horizontal, vertical] or just a number for equal margins
      pageMargins: [8, 4, 2, 4],
      content: [
        { text: 'Swintec Computer Institute,', style: 'header' },
        {
          text: 'JAMB ACCREDITED REG/EXAM CENTRE',
          style: 'subheader',
          alignment: 'center'
        },
        { text: '5/9 Hospital/Orlu Rd., Owerri.', alignment: 'center' },
        { text: '(Call: 0802-975-4376 OR 0806-362-0194)', alignment: 'center' },

        {
          text: 'Queue Number: ' + queueNo,
          style: 'header',
          alignment: 'center'
        },
        { text: 'Date: ' + this.miscKit.getNowAsString() },

        { text: 'Printed by: ' + this.$UID, style: 'subheader' }
      ],
      styles: {
        header: {
          fontSize: 14,
          bold: true,
          margin: [0, 0, 0, 1],
          alignment: 'center'
        },
        subheader: {
          fontSize: 12,
          bold: true,
          margin: [0, 1, 0, 1]
        }
      },
      defaultStyle: {
        fontSize: 10
      }
    };

    return dd;
  }
}
