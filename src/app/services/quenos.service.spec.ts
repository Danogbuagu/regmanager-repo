import { TestBed, inject } from '@angular/core/testing';

import { QuenosService } from './quenos.service';

describe('QuenosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuenosService]
    });
  });

  it('should be created', inject([QuenosService], (service: QuenosService) => {
    expect(service).toBeTruthy();
  }));
});
