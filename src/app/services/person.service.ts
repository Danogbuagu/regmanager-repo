import { Injectable, NgZone } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { StringsKit } from '../helpers/strings-kit';
import { throwError, Observable } from 'rxjs';
import { Person } from '../models/person.interface';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  person: Person = new Person();

  constructor(
    private http: HttpClient,
    private strkit: StringsKit,
    private zone: NgZone
  ) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer {JWT Token}'
    })
  };

  endpointUrl = 'http://localhost:9900/person';

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a person-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  getPersons(): Observable<Object> {
    return this.http.get(this.endpointUrl);
  }

  /** POST: add a new person to the database */
  createNew(person: Person): Observable<Person> {
    person.fname = this.strkit.toTitleCase(person.fname);
    person.onames = this.strkit.toTitleCase(person.onames);
    person.sname = person.sname.toUpperCase();

    console.log(
      `Person object arriving local service: ${JSON.stringify(person)}`
    );

    const ret = this.http.post<Person>(
      this.endpointUrl,
      person,
      this.httpOptions
    );
    ret.subscribe(e => {
      console.log(
        `Maybe saved records for ${e.sname}, ${e.fname} ${
          e.onames
        } to database.`
      );
    });
    return ret;
  }

  getPersonByPhoneNo(phoneNo: string): Person {
    console.log(
      `...... getPersonByPhoneNo() accessed with Person ID of ${phoneNo} ..........`
    );

    let arr: Person[];

    const allPersons: Observable<Object> = this.getPersons();

    allPersons.subscribe(pers => {
      //  console.log(`All persons: ${JSON.stringify(pers)}`);
      this.zone.run(() => {
        arr = pers as Person[];
        // arr.forEach(it => {
        //   console.log(`Person in array:${it.sname}, ${it.fname} ${it.onames} `);
        // });
        this.person = arr.find(_ => _.phoneNo === phoneNo);
        console.log(
          `Person found: ${this.person.sname}, ${this.person.fname} ${
            this.person.onames
          } `
        );
      });
    });
    return this.person;
  }
}
