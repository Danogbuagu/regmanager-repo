import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home', canActivate: [AuthGuard],
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'home/:id', canActivate: [AuthGuard],
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  {
    path: 'user', canActivate: [AuthGuard],
    loadChildren: './pages/user/user/user.module#UserPageModule'
  },
  {
    path: 'userlist', canActivate: [AuthGuard],
    loadChildren: './pages/user/userlist/userlist.module#UserlistPageModule'
  },
  {
    path: 'userprofile', canActivate: [AuthGuard],
    loadChildren:
      './pages/user/userprofile/userprofile.module#UserprofilePageModule'
  },
  {
    path: 'print-qnos', canActivate: [AuthGuard],
    loadChildren: './pages/print-qnos/print-qnos.module#PrintQnosPageModule'
  },
  {
    path: 'call-qnos', canActivate: [AuthGuard],
    loadChildren: './pages/call-qnos/call-qnos.module#CallQnosPageModule'
  },
  {
    path: 'add-person', canActivate: [AuthGuard],
    loadChildren: './pages/add-person/add-person.module#AddPersonPageModule'
  },
  {
    path: 'sell-form', canActivate: [AuthGuard],
    loadChildren: './pages/sell-form/sell-form.module#SellFormPageModule'
  },
  {
    path: 'sales-list', canActivate: [AuthGuard],
    loadChildren: './pages/sales-list/sales-list.module#SalesListPageModule'
  },
  {
    path: 'sales-report', canActivate: [AuthGuard],
    loadChildren:
      './pages/sales-report/sales-report.module#SalesReportPageModule'
  },
  {
    path: 'pin-vend', canActivate: [AuthGuard],
    loadChildren: './pages/pin-vend/pin-vend.module#PinVendPageModule'
  },
  {
    path: 'gen-qnos', canActivate: [AuthGuard],
    loadChildren: './pages/gen-qnos/gen-qnos.module#GenQnosPageModule'
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'logout', loadChildren: './logout/logout.module#LogoutPageModule' },
  {
    path: 'sales-rpt-general', canActivate: [AuthGuard],
    loadChildren:
      './pages/sales-rpt-general/sales-rpt-general.module#SalesRptGeneralPageModule'
  },
  { path: 'add-bizgroup', canActivate: [AuthGuard],  loadChildren: './pages/add-bizgroup/add-bizgroup.module#AddBizgroupPageModule' },
  { path: 'expense-report', canActivate: [AuthGuard],
  loadChildren: './pages/expense-report/expense-report.module#ExpenseReportPageModule' },
  { path: 'pin-vending', canActivate: [AuthGuard],  loadChildren: './pages/pin-vending/pin-vending.module#PinVendingPageModule' },
  { path: 'daily-expenses', canActivate: [AuthGuard],
  loadChildren: './pages/daily-expenses/daily-expenses.module#DailyExpensesPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
