import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellFormPage } from './sell-form.page';

describe('SellFormPage', () => {
  let component: SellFormPage;
  let fixture: ComponentFixture<SellFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellFormPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
