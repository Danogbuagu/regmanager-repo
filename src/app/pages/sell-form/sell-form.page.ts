import { Component, OnInit, NgZone } from '@angular/core';
import { Person } from '../../models/person.interface';
import { Constants } from '../../models/constants';
import { PersonService } from '../../services/person.service';
import { AlertUtils } from '../../helpers/alert-utils';
import { Observable } from 'rxjs';
import { ReceiptData } from '../../models/receipt-data.interface';
import { ReceiptsService } from '../../services/receipts.service';
import { AlertController } from '@ionic/angular';
import * as pdfMake from 'pdfmake/build/pdfmake';
import { BizgroupsService } from '../../services/bizgroups.service';
import { Bizgroup } from '../../models/bigroup.interface';
import { User } from '../../models/user';

@Component({
  selector: 'app-sell-form',
  templateUrl: './sell-form.page.html',
  styleUrls: ['./sell-form.page.scss']
})
export class SellFormPage implements OnInit {
  public loggedInUser: User = new User();

  submitAttempt = false;
  controlsValid = false;

  searchQuery = '';
  phoneNos = [''];
  price = 0;
  formNo = '';
  group = '';
  //  selectedPerson: Person = new Person();
  $UID = 'staff@swintec.com';

  amountsObj = new Constants();
  jamb_amt = this.amountsObj.jambAmt();
  service_charge = this.amountsObj.regAmt();

  selectedPerson: Person = new Person();

  receiptData = {
    sname: '',
    fname: '',
    onames: '',
    phoneNo: '',
    receiptNo: '',
    loggedInUser: '',
    receiptDate: new Date().toISOString().slice(0, 10),
    formNo: '',
    amount: 0.0,
    jamb: 0.0,
    svc_charge: 0.0,
    group: ''
  };

  groups = [''];
  price_grps: Bizgroup[];

  priceConsts = new Constants();
  persons = new Observable<Object>();
  persArr = new Array<Person>();

  constructor(
    private personSvc: PersonService,
    public rcptDatSvc: ReceiptsService,
    public kit: AlertUtils,
    private grpsSvc: BizgroupsService,
    private alertCtrl: AlertController,
    private zone: NgZone
  ) {
    this.receiptData.formNo = this.formNo;
    this.receiptData.group = this.group;

    this.receiptData.jamb = Number(this.jamb_amt).valueOf();
    this.receiptData.svc_charge = this.getServiceCharge(this.price);

    this.phoneNos = [''];
    //  this.initializeItems();
  }

  ngOnInit() {
    this.initializeItems();
  }

  getServiceCharge(selectedPrice: number): number {
    if (selectedPrice > this.priceConsts.jambAmt()) {
      this.service_charge = selectedPrice - this.priceConsts.jambAmt();
    } else {
      this.service_charge = selectedPrice;
      // this.jamb_amt = 0.0;
    }
    return Number(this.service_charge).valueOf();
  }

  getJambAmt(selectedPrice: number): number {
    if (selectedPrice > this.priceConsts.jambAmt()) {
      this.jamb_amt = this.priceConsts.jambAmt();
      // this.service_charge = selectedPrice - this.priceConsts.jambAmt;
    } else {
      // this.service_charge = selectedPrice;
      this.jamb_amt = 0.0;
    }
    return Number(this.jamb_amt).valueOf();
  }

  initializeItems() {
    const groups$ = this.grpsSvc.getBizgroups();
    groups$.subscribe(grp => {
      this.zone.run(() => {
        const arr = grp as Bizgroup[];
        this.groups = arr.map(_ => _.groupname);
        this.groups.unshift('SWINTEC');
        this.price_grps = arr;
        this.price_grps.unshift({
          groupname: 'SWINTEC',
          formAmount: 6500,
          serviceCharge: 700
        });
      });
    });

    // grp_prices

    this.persons = this.personSvc.getPersons();
    this.persons.subscribe(pers => {
      this.zone.run(() => {
        const arr = pers as Person[];
        this.persArr = arr;
        this.phoneNos = arr.map(_ => _.phoneNo);
      });
    });
  }

  accept(phoneNo: string) {
    this.searchQuery = phoneNo;

    this.kit.presentToast('I selected ' + this.searchQuery, 4000);

    this.loadPerson(phoneNo);
    this.initializeItems();
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    // this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() !== '') {
      this.phoneNos = this.phoneNos.filter(phon => {
        return phon.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
    }
  }

  async loadPerson(phoneNo: string) {
    let person = new Person();
    console.log(`loadPerson() called with Phone no: ${phoneNo}`);

    person = this.persArr.find(_ => _.phoneNo === phoneNo);
    this.selectedPerson = person;
    console.log(
      `Person found: ${person.sname}, ${person.fname} ${person.onames} `
    );

    console.log(
      `Person found: ${person.sname}, ${person.fname} ${
        person.onames
      } with Phone No: ${person.phoneNo}.`
    );

    if (person) {
      console.log(
        `Person found: ${person.sname}, ${person.fname} ${
          person.onames
        } with Phone No: ${person.phoneNo}.`
      );
      if (!this.price || !this.formNo || !this.group) {
        const hdr = 'Necessary Data Missing!';
        const subHdr = 'Unknown Phone Number';
        const msg =
          'Please select the missing price, group or form number, first.';
        this.kit.presentAlert(hdr, subHdr, msg);
        return;
      } else {
        if (this.group.toUpperCase() === 'SWINTEC') {
          this.receiptData.svc_charge = Number(
            this.priceConsts.regAmt
          ).valueOf();
        } else {
          this.receiptData.svc_charge = this.getServiceCharge(this.price);
        }
        this.receiptData.fname = person.fname;
        this.receiptData.sname = person.sname;
        this.receiptData.onames = person.onames;
        this.receiptData.phoneNo = person.phoneNo;
        this.receiptData.receiptDate = new Date().toISOString().slice(0, 10);
        console.log(
          `Receipt Date to save id ${JSON.stringify(
            this.receiptData.receiptDate
          )}`
        );
        this.receiptData.loggedInUser = this.$UID;
        this.receiptData.formNo = this.formNo;
        this.receiptData.receiptNo = this.formNo;
        this.receiptData.amount = Number(this.price).valueOf();
        this.receiptData.svc_charge = Number(
          this.getServiceCharge(this.price)
        ).valueOf();
        this.receiptData.jamb = Number(this.getJambAmt(this.price)).valueOf();
        this.receiptData.group = this.group;

        await this.printReceipt(this.receiptData, Number(this.price).valueOf());

        await this.initializeItems();

        await this.resetForm();

        return;
      }
    } else {
      const hdr = 'Error: Missing Record!';
      const subHdr = 'Unknown Phone Number';
      const msg = 'Cannot locate person with this phone number :' + phoneNo;
      this.kit.presentAlert(hdr, subHdr, msg);
      return;
    }
  }

  resetForm() {
    this.price = 0;
    this.formNo = '';
    this.group = '';
  }

  async printReceipt(receiptData: ReceiptData, price: number) {
    console.log(
      `Calling printReceipt with ${JSON.stringify(receiptData)} and phone no: ${
        this.searchQuery
      }`
    );
    const docDefinition = await this.rcptDatSvc.createDocDefinition(
      receiptData,
      price
    );
    // open the PDF in a new window
    // pdfMake.createPdf(docDefinition).open();

    const alertTitle = 'Confirmation Required!';
    const alertMsg =
      'You want to print Receipt \nand also save the transaction?';

    const alert = await this.alertCtrl.create({
      header: alertTitle,
      message: alertMsg,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.kit.presentToast(
              'Receipt NOT printing ... \naction rejected by user!',
              4 * 1000
            );
          }
        },
        {
          text: 'Sell Form',
          handler: () => {
            // print the PDF
            pdfMake.createPdf(docDefinition).print();

            // save the transaction record
            this.rcptDatSvc.saveReceipt(receiptData);
            this.kit.presentToast(
              'Receipt printing ... \nand transactions to be saved.',
              4 * 1000
            );
          }
        }
      ]
    });
    await alert.present();
  }
}
