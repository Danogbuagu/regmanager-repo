import { Component, OnInit } from '@angular/core';
import { QuenosService } from '../../services/quenos.service';
import { Observable } from 'rxjs';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { User } from '../../models/user';

@Component({
  selector: 'app-gen-qnos',
  templateUrl: './gen-qnos.page.html',
  styleUrls: ['./gen-qnos.page.scss']
})
export class GenQnosPage implements OnInit {
  public loggedInUser: User = new User();

  queueNos: Observable<Object>;
  numgenForm: FormGroup;
  submitAttempt = false;

  constructor(private queSvc: QuenosService, private formBuilder: FormBuilder) {
    this.queueNos = this.queSvc.getQuenumbers();
    this.clearEntries();
  }

  async generateNumbers() {
    this.submitAttempt = true;
    if (!this.numgenForm.valid) {
      console.log(`Form is invalid`);
    } else {
      const fromNo = this.numgenForm.value.fromNo;
      const toNo = this.numgenForm.value.toNo;
      await this.queSvc.generateNumbers(
        parseInt(fromNo, 10),
        parseInt(toNo, 10)
      );
    }
  }

  ngOnInit() {}

  clearEntries() {
    this.numgenForm = this.formBuilder.group({
      fromNo: [1001, Validators.compose([Validators.required])],
      toNo: [1500, Validators.compose([Validators.required])]
    });
  }
}
