import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Person } from '../../models/person.interface';
import { Router } from '@angular/router';
import { PersonService } from '../../services/person.service';
import { Constants } from '../../models/constants';
import { AlertUtils } from '../../helpers/alert-utils';
import { StringsKit } from '../../helpers/strings-kit';
import { User } from '../../models/user';

@Component({
  selector: 'app-add-person',
  templateUrl: './add-person.page.html',
  styleUrls: ['./add-person.page.scss']
})
export class AddPersonPage implements OnInit {
  personForm: FormGroup;
  submitAttempt = false;
  controlsValid = false;
  public loggedInUser: User = new User();
  
  newPerson: Person = new Person();
  amountsObj = new Constants();
  constructor(
    private formBuilder: FormBuilder,
    private userSvc: PersonService,
    public kit: AlertUtils,
    private strkit: StringsKit,
    private route: Router
  ) {
    this.clearEntries();
    this.personForm.value.sname = this.personForm.value.sname.toUpperCase();
    this.personForm.value.fname = this.strkit.toTitleCase(
      this.personForm.value.fname
    );
    this.personForm.value.onames = this.strkit.toTitleCase(
      this.personForm.value.onames
    );
  }

  ngOnInit() {
    // this.users = this.userSvc.getUsers();
  }

  renderCase(item: string) {
    item = item.toUpperCase();
  }

  toTitleCase(item: string) {
    item = this.strkit.toTitleCase(item);
  }

  async register() {
    this.submitAttempt = true;
    if (!this.personForm.valid) {
      console.log(`Form is invalid`);
    } else {
      this.personForm.value.sname = this.personForm.value.sname;

      const sname: string = this.personForm.value.sname;
      const fname: string = this.personForm.value.fname;
      const onames: string = this.personForm.value.onames;
      const phoneNo: string = this.personForm.value.phoneNo;
      const jambAmt: number = this.amountsObj.jambAmt();
      const regAmt: number = this.amountsObj.regAmt();

      this.newPerson = {
        sname: sname.toUpperCase(),
        fname: this.strkit.toTitleCase(fname),
        onames: this.strkit.toTitleCase(onames),
        phoneNo: phoneNo,
        jambAmt: jambAmt,
        regAmt: regAmt
      };
      await this.userSvc.createNew(this.newPerson);
      const msg =
        sname.toUpperCase() +
        ', ' +
        this.strkit.toTitleCase(fname) +
        ' ' +
        this.strkit.toTitleCase(onames) +
        ' added!';
      await this.kit.presentToast(msg, 4 * 1000);
      this.submitAttempt = false;
      // await this.route.navigateByUrl('/add-person');
      this.clearEntries();
    }
  }

  clearEntries() {
    this.personForm = this.formBuilder.group({
      sname: [
        '',
        Validators.compose([
          Validators.maxLength(40),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ],
      fname: [
        '',
        Validators.compose([
          Validators.maxLength(40),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ],
      onames: [''],
      phoneNo: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(9),
          Validators.maxLength(11)
        ])
      ]
    });
  }
}
