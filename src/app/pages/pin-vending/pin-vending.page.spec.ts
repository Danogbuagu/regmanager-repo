import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PinVendingPage } from './pin-vending.page';

describe('PinVendingPage', () => {
  let component: PinVendingPage;
  let fixture: ComponentFixture<PinVendingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PinVendingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PinVendingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
