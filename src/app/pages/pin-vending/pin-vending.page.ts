import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';

@Component({
  selector: 'app-pin-vending',
  templateUrl: './pin-vending.page.html',
  styleUrls: ['./pin-vending.page.scss'],
})
export class PinVendingPage implements OnInit {
  public loggedInUser: User = new User();

  constructor() { }

  ngOnInit() {
  }

}
