import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Bizgroup } from '../../models/bigroup.interface';
import { BizgroupsService } from '../../services/bizgroups.service';
import { AlertUtils } from '../../helpers/alert-utils';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { User } from '../../models/user';

@Component({
  selector: 'app-add-bizgroup',
  templateUrl: './add-bizgroup.page.html',
  styleUrls: ['./add-bizgroup.page.scss']
})
export class AddBizgroupPage implements OnInit {
  groups: Observable<Object>;
  groupForm: FormGroup;
  submitAttempt = false;
  controlsValid = false;
  public loggedInUser: User = new User();

  newBizgroup: Bizgroup = new Bizgroup();
  constructor(
    private formBuilder: FormBuilder,
    private grpSvc: BizgroupsService,
    private router: Router,
    public kit: AlertUtils
  ) {
    this.clearEntries();
    this.groupForm.value.groupname = this.groupForm.value.groupname.toUpperCase();
  }

  ngOnInit() {
    this.groups = this.grpSvc.getBizgroups();
  }

  async save() {
    this.submitAttempt = true;
    if (!this.groupForm.valid) {
      console.log(`Form is invalid`);
    } else {
      this.groupForm.value.groupname = this.groupForm.value.groupname;

      const groupname: string = this.groupForm.value.groupname;
      const formAmount: number = this.groupForm.value.formAmount;
      const serviceCharge: number = this.groupForm.value.serviceCharge;

      this.newBizgroup = {
        groupname: groupname.toUpperCase(),
        formAmount: formAmount,
        serviceCharge: serviceCharge
      };
      await this.grpSvc.createNew(this.newBizgroup);
      const msg = `${groupname.toUpperCase()}, Form price: ${formAmount} and Service Charge; ${serviceCharge} added!`;
      await this.kit.presentToast(msg, 4 * 1000);
      this.submitAttempt = false;
      await this.clearEntries();
      await this.router.navigateByUrl('/add-bizgroup');
    }
  }

  clearEntries() {
    this.groupForm = this.formBuilder.group({
      groupname: [
        '',
        Validators.compose([
          Validators.maxLength(40),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ],
      formAmount: [0.0, Validators.compose([Validators.required])],
      serviceCharge: [0.0, Validators.compose([Validators.required])]
    });
  }
}
