import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBizgroupPage } from './add-bizgroup.page';

describe('AddBizgroupPage', () => {
  let component: AddBizgroupPage;
  let fixture: ComponentFixture<AddBizgroupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBizgroupPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBizgroupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
