import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PinVendPage } from './pin-vend.page';

describe('PinVendPage', () => {
  let component: PinVendPage;
  let fixture: ComponentFixture<PinVendPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PinVendPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PinVendPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
