import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';

@Component({
  selector: 'app-pin-vend',
  templateUrl: './pin-vend.page.html',
  styleUrls: ['./pin-vend.page.scss'],
})
export class PinVendPage implements OnInit {
  public loggedInUser: User = new User();
  constructor() { }

  ngOnInit() {
  }

}
