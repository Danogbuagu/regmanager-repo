import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesListPage } from './sales-list.page';

describe('SalesListPage', () => {
  let component: SalesListPage;
  let fixture: ComponentFixture<SalesListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
