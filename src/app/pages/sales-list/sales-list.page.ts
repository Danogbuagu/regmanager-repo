import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ReceiptsService } from '../../services/receipts.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-sales-list',
  templateUrl: './sales-list.page.html',
  styleUrls: ['./sales-list.page.scss'],
})
export class SalesListPage implements OnInit {
  public loggedInUser: User = new User();
  
  public rcpts: Observable<Object>;

  constructor(private rcptSvc: ReceiptsService) {}

  ngOnInit() {
    this.rcpts = this.rcptSvc.getReceipts();
  }
}
