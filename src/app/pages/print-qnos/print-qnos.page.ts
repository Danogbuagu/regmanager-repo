import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { QuenosService } from '../../services/quenos.service';
import { Quenumber } from '../../models/quenumber';

import * as pdfMake from 'pdfmake/build/pdfmake';
import { Router } from '@angular/router';
import { AlertUtils } from '../../helpers/alert-utils';
import { User } from '../../models/user';

@Component({
  selector: 'app-print-qnos',
  templateUrl: './print-qnos.page.html',
  styleUrls: ['./print-qnos.page.scss']
})
export class PrintQnosPage implements OnInit {
  public loggedInUser: User = new User();

  queueNos: Observable<Object>;
  selectedNumber: Quenumber;
  items = new Array<string>();

  constructor(
    private queSvc: QuenosService,
    private router: Router,
    private alertKit: AlertUtils
  ) {}

  ngOnInit() {
    this.queueNos = this.queSvc.getPrtnumbers();
  }

  async printNext(numId: Quenumber) {
    await this.printNumber(numId.queno.toString());
    //  await this.queSvc.deleteQueno(numId.queno.toString());
    await this.queSvc.deletePrtno(numId.queno.toString());
    await this.alertKit.presentToast(
      'Queue Number printing ... \nand transactions to be saved.',
      4 * 1000
    );
    await this.router.navigateByUrl('/print-qnos');
  }

  printNumber(queno: string): boolean {
    const docDefinition = this.queSvc.createDocDefinition(queno);

    // print the PDF
    pdfMake.createPdf(docDefinition).print();

    return true;
  }
}
