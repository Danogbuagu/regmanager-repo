import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintQnosPage } from './print-qnos.page';

describe('PrintQnosPage', () => {
  let component: PrintQnosPage;
  let fixture: ComponentFixture<PrintQnosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintQnosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintQnosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
