import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SalesRptGeneralPage } from './sales-rpt-general.page';

const routes: Routes = [
  {
    path: '',
    component: SalesRptGeneralPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SalesRptGeneralPage]
})
export class SalesRptGeneralPageModule {}
