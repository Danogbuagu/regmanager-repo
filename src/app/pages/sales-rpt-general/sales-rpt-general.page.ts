import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';

@Component({
  selector: 'app-sales-rpt-general',
  templateUrl: './sales-rpt-general.page.html',
  styleUrls: ['./sales-rpt-general.page.scss'],
})
export class SalesRptGeneralPage implements OnInit {
  public loggedInUser: User = new User();

  constructor() { }

  ngOnInit() {
  }

}
