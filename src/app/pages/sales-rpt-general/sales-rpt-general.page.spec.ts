import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesRptGeneralPage } from './sales-rpt-general.page';

describe('SalesRptGeneralPage', () => {
  let component: SalesRptGeneralPage;
  let fixture: ComponentFixture<SalesRptGeneralPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesRptGeneralPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesRptGeneralPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
