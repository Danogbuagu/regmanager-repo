import { Component, OnInit, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { ReceiptsService } from '../../services/receipts.service';
import { SalesRptPrintService } from '../../services/sales-rpt-print.service';
import { AlertController } from '@ionic/angular';
import { AlertUtils } from '../../helpers/alert-utils';
import { ReceiptData } from '../../models/receipt-data.interface';
import { MiscUtilsKit } from '../../helpers/misc-utils-kit';
import { User } from '../../models/user';

@Component({
  selector: 'app-sales-report',
  templateUrl: './sales-report.page.html',
  styleUrls: ['./sales-report.page.scss']
})
export class SalesReportPage implements OnInit {
  public loggedInUser: User = new User();

  rptDate: string = new Date().toISOString().slice(0, 10);
  receipts$: Observable<Object>;
  receiptArr = new Array<ReceiptData>();

  jsonRows = [];
  jambArr = [0.0];
  svcArr = [0.0];
  amtArr = [0.0];

  totJamb = 0.0;
  totSvcCharge = 0;
  totAmt = 0.0;

  numJamb = 0;
  numForms = 0;

  constructor(
    public rcptSvc: ReceiptsService,
    public alertController: AlertController,
    private kit: AlertUtils,
    private miscKit: MiscUtilsKit,
    private zone: NgZone,
    public salesRptSvc: SalesRptPrintService
  ) {
    this.rptDate = this.rptDate.slice(0, 10);
  }

  ngOnInit() {
    this.rptDate = new Date().toISOString().slice(0, 10);
    console.log(`Starting date as ISO string is ${this.rptDate}`);
  }

  doPrintHTML() {
    const toPrint = document.getElementById('print-area');

    const popupWin = window.open(
      '',
      '_blank',
      'width=1200,height=800,location=no,left=100px'
    );

    popupWin.document.open();

    popupWin.document.write('<html><head>');
    popupWin.document.write('<title>');
    // popupWin.document.write(this.fileName);
    popupWin.document.write('</title>');

    popupWin.document.write(`<link rel="stylesheet" type="text/css"
          href="sales-report.scss" media="print" />`);
    popupWin.document.write('</head>');
    popupWin.document.write('<body onload="window.print()" >');

    popupWin.document.write(toPrint.innerHTML);

    popupWin.document.write('<div style="width:100%;text-align:right">');
    popupWin.document.write(
      '<input type="button" id="btnCancel" value="Close" class="no-print"  style="width:100px" onclick="window.close()" />'
    );
    popupWin.document.write('</body>');
    popupWin.document.write('</html>');

    popupWin.document.close();
  }

  calcTotJAMB(): number {
    if (this.jambArr.length > 0) {
      return this.jambArr.reduce(function(sum, value) {
        return sum + value;
      }, 0);
    } else {
      return 0.0;
    }
  }

  calcNumJAMB(): number {
    if (this.jambArr.length > 0) {
      return this.jambArr.length;
    } else {
      return 0;
    }
  }

  calcSvcCharge(): number {
    if (this.svcArr.length > 0) {
      return this.svcArr.reduce(function(sum, value) {
        return sum + value;
      }, 0);
    } else {
      return 0.0;
    }
  }

  calcTotAmt(): number {
    return this.calcTotJAMB() + this.calcSvcCharge();
  }

  async printReport() {
    if (this.rptDate) {
      let dtt = JSON.stringify(this.rptDate);
      //    console.log(`Captured value of date in printReport(): ${dtt}`);
      dtt = JSON.parse(dtt);
      const yy = dtt['year'].value;
      console.log(`Captured value of Year in printReport(): ${yy}`);
      const mm = dtt['month'].value;
      console.log(`Captured value of Year in printReport(): ${mm}`);
      const dd = dtt['day'].value;
      console.log(`Captured value of Year in printReport(): ${dd}`);

      const date: Date = new Date(
        parseInt(yy, 10),
        parseInt(mm, 10) - 1,
        parseInt(dd, 10) + 1
      );

      // this.rptDate = this.miscKit
      //   .isoToDateObject(this.rptDate)
      //   .toISOString()
      //   .slice(0, 10);

      this.rptDate = date.toISOString().slice(0, 10);

      console.log(
        `Captured value of Report Date in printReport(): ${this.rptDate}`
      );
      const alertTitle = 'Confirmation Required!';
      const alertMsg = 'You want to print a Sales Report?';

      const alert = await this.alertController.create({
        header: alertTitle,
        message: alertMsg,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              this.kit.presentToast(
                'Report NOT printing ... \naction rejected by user!',
                4 * 1000
              );
            }
          },
          {
            text: 'Go ahead, Print',
            handler: () => {
              let arr = new Array();
              const jsonFriendlyStringify = (s: string): any => {
                return JSON.stringify(s, [
                  'formNo',
                  'receiptDate',
                  'sname',
                  'fname',
                  'onames',
                  'phoneNo',
                  'group',
                  'jamb',
                  'svc_charge',
                  'amount'
                ])
                  .replace(/\u2028/g, '\\u2028')
                  .replace(/\u2029/g, '\\u2029')
                  .replace(/\"(\w+)\"\:/g, '$1:');
              };
              console.log(
                `Value of rptDate going into receipt service from page module => ${
                  this.rptDate
                }`
              );
              const receipts2$ = this.rcptSvc.getReceiptsOnDate(this.rptDate);
              receipts2$.subscribe(items => {
                this.zone.run(() => {
                  this.receiptArr = items as ReceiptData[];
                  this.receiptArr.forEach(it => {
                    this.jsonRows.push({
                      formNo: it.formNo,
                      receiptDate: it.receiptDate.slice(0, 10),
                      sname: it.sname,
                      fname: it.fname,
                      onames: it.onames,
                      phoneNo: it.phoneNo,
                      group: it.group,
                      jamb: it.jamb.valueOf().toFixed(2),
                      svc_charge: it.svc_charge.valueOf().toFixed(2),
                      amount: it.amount.valueOf().toFixed(2)
                    });

                    this.jambArr.push(Number(it.jamb).valueOf());
                    this.svcArr.push(Number(it.svc_charge).valueOf());
                    this.amtArr.push(Number(it.amount).valueOf());
                  });

                  arr = new Array();
                  for (const i of this.jsonRows) {
                    console.log('Showing: ' + jsonFriendlyStringify(i));
                    const r = jsonFriendlyStringify(i);
                    arr.push(r);
                  }

                  this.totJamb = this.calcTotJAMB();
                  this.totSvcCharge = this.calcSvcCharge();
                  this.totAmt = this.calcTotAmt();

                  this.numJamb = this.calcNumJAMB();
                  this.numForms = arr.length;

                  //
                });
                this.doPrintHTML();
              });
              // this.doPrintHTML();
              // this.doPrintHTML();
              this.kit.presentToast('Report printing ...  ', 4 * 1000);
            }
          }
        ]
      });
      alert.present();
    } else {
      this.kit.presentAlert(
        'Missing Option(s)',
        'Check Report date',
        'Date is missing\nSupply missing data, to proceed. '
      );
      return;
    }
  }
}
