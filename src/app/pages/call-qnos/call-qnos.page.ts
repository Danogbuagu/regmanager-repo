import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { QuenosService } from '../../services/quenos.service';
import { Quenumber } from '../../models/quenumber';

import { BasicSpeechOptions } from '../../models/basic-speech-options';
import { ModalController } from '@ionic/angular';
import { User } from '../../models/user';

declare var require: any;
const meSpeak = require('mespeak');

@Component({
  selector: 'app-call-qnos',
  templateUrl: './call-qnos.page.html',
  styleUrls: ['./call-qnos.page.scss']
})
export class CallQnosPage implements OnInit {

  public loggedInUser: User = new User();

  queueNos: Observable<Object>;

  speakOptions: BasicSpeechOptions = {
    amplitude: 100,
    pitch: 50,
    speed: 175,
    voice: '',
    wordgap: 0,
    variant: 'f2'
  };

  constructor(
    private queSvc: QuenosService,
    public modalCtrl: ModalController
  ) {}

  ngOnInit() {
    meSpeak.loadVoice(require('mespeak/voices/en/en-us.json'));
    this.speakOptions = {
      amplitude: 100,
      pitch: 60,
      speed: 150,
      voice: '',
      wordgap: 20,
      variant: 'f5'
    };
    this.queueNos = this.queSvc.getQuenumbers();
  }

  callNextPerson(numId: Quenumber) {
    // await setTimeout(
    //   meSpeak.speak('Number ' + numId.queno, this.speakOptions, callback => {}),
    //   3000
    // );

    meSpeak.speak('Number ' + numId.queno, this.speakOptions);

    // await setTimeout(
    //   meSpeak.speak('Number ' + numId.queno, this.speakOptions, callback => {}),
    //   3000
    // );

    // await setTimeout(
    //   meSpeak.speak('Number ' + numId.queno, this.speakOptions, callback => {}),
    //   3000
    // );

    this.queSvc.deleteQueno(numId.queno.toString());
  }
}
