import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallQnosPage } from './call-qnos.page';

describe('CallQnosPage', () => {
  let component: CallQnosPage;
  let fixture: ComponentFixture<CallQnosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallQnosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallQnosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
