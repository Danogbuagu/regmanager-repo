import { Component, OnInit, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { DailyExpensesService } from '../../services/daily-expenses.service';
import { AlertController } from '@ionic/angular';
import { AlertUtils } from '../../helpers/alert-utils';
import { MiscUtilsKit } from '../../helpers/misc-utils-kit';
import { User } from '../../models/user';

class ExpenseData {
  expenseDate: string;
  category: string;
  expenseDetails: string;
  amount: number;
  loggedInUser: string;
  approvedBy: string;
  constructor() {
    this.expenseDate = this.expenseDate.slice(0, 10);
  }
}

@Component({
  selector: 'app-expense-report',
  templateUrl: './expense-report.page.html',
  styleUrls: ['./expense-report.page.scss']
})
export class ExpenseReportPage implements OnInit {
  public loggedInUser: User = new User();

  expDate: string = new Date().toISOString().slice(0, 10);
  expenses$: Observable<Object>;
  expenseArr = new Array<ExpenseData>();
  jsonRows = [];
  amtArr = [0.0];
  totAmt = 0.0;

  constructor(
    public expSvc: DailyExpensesService,
    public alertController: AlertController,
    private kit: AlertUtils,
    private zone: NgZone,
    public expRptSvc: DailyExpensesService
  ) {
    this.expDate = this.expDate.slice(0, 10);
  }

  ngOnInit() {
    this.expDate = new Date().toISOString().slice(0, 10);
    console.log(`Starting date as ISO string is ${this.expDate}`);
  }

  doPrintHTML() {
    const toPrint = document.getElementById('print-area');

    const popupWin = window.open(
      '',
      '_blank',
      'width=1200,height=800,location=no,left=100px'
    );

    popupWin.document.open();

    popupWin.document.write('<html><head>');
    popupWin.document.write('<title>');
    // popupWin.document.write(this.fileName);
    popupWin.document.write('</title>');

    popupWin.document.write(`<link rel="stylesheet" type="text/css"
          href="sales-report.scss" media="print" />`);
    popupWin.document.write('</head>');
    popupWin.document.write('<body onload="window.print()" >');

    popupWin.document.write(toPrint.innerHTML);

    popupWin.document.write('<div style="width:100%;text-align:right">');
    popupWin.document.write(
      '<input type="button" id="btnCancel" value="Close" class="no-print"  style="width:100px" onclick="window.close()" />'
    );
    popupWin.document.write('</body>');
    popupWin.document.write('</html>');

    popupWin.document.close();
  }

  async printReport() {
    if (this.expDate) {
      let dtt = JSON.stringify(this.expDate);
      //    console.log(`Captured value of date in printReport(): ${dtt}`);
      dtt = JSON.parse(dtt);
      const yy = dtt['year'].value;
      console.log(`Captured value of Year in printReport(): ${yy}`);
      const mm = dtt['month'].value;
      console.log(`Captured value of Year in printReport(): ${mm}`);
      const dd = dtt['day'].value;
      console.log(`Captured value of Year in printReport(): ${dd}`);

      const date: Date = new Date(
        parseInt(yy, 10),
        parseInt(mm, 10) - 1,
        parseInt(dd, 10) + 1
      );

      this.expDate = date.toISOString().slice(0, 10);

      console.log(
        `Captured value of Report Date in printReport(): ${this.expDate}`
      );
      const alertTitle = 'Confirmation Required!';
      const alertMsg = 'You want to print an Expenses Report?';

      const alert = await this.alertController.create({
        header: alertTitle,
        message: alertMsg,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              this.kit.presentToast(
                'Report NOT printing ... \naction rejected by user!',
                4 * 1000
              );
            }
          },
          {
            text: 'Go ahead, Print',
            handler: () => {
              let arr = new Array();
              const jsonFriendlyStringify = (s: string): any => {
                return JSON.stringify(s, [
                  'expenseDate',
                  'category',
                  'expenseDetails',
                  'amount',
                  'loggedInUser',
                  'approvedBy'
                ])
                  .replace(/\u2028/g, '\\u2028')
                  .replace(/\u2029/g, '\\u2029')
                  .replace(/\"(\w+)\"\:/g, '$1:');
              };
              console.log(
                `Value of expDate going into expenses service from page module => ${
                  this.expDate
                }`
              );
              const receipts2$ = this.expSvc.getExpensesOnDate(this.expDate);
              receipts2$.subscribe(items => {
                this.zone.run(() => {
                  this.expenseArr = items as ExpenseData[];
                  this.expenseArr.forEach(it => {
                    this.jsonRows.push({
                      expenseDate: it.expenseDate.slice(0, 10),
                      category: it.category,
                      expenseDetails: it.expenseDetails,
                      amount: it.amount.valueOf().toFixed(2),
                      loggedInUser: it.loggedInUser,
                      approvedBy: it.approvedBy
                    });

                    this.amtArr.push(Number(it.amount).valueOf());
                    console.log(`Showing: ${JSON.stringify(it)}`);
                  });

                  arr = new Array();
                  for (const i of this.jsonRows) {
                    console.log('Showing: ' + jsonFriendlyStringify(i));
                    const r = jsonFriendlyStringify(i);
                    arr.push(r);

                    console.log(`Showing In jsonArray: ${JSON.stringify(i)}`);
                  }
                  this.totAmt = this.amtArr.reduce(
                    (total, amount) => total + amount
                  );
                });
                this.doPrintHTML();
              });

              this.kit.presentToast('Report printing ...  ', 4 * 1000);
            }
          }
        ]
      });
      alert.present();
    } else {
      this.kit.presentAlert(
        'Missing Option(s)',
        'Check Report date',
        'Date is missing\nSupply missing data, to proceed. '
      );
      return;
    }
  }
}
