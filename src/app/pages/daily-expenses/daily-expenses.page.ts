import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DailyExpenses } from '../../models/daily-expenses.interface';
import { DailyExpensesService } from '../../services/daily-expenses.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-daily-expenses',
  templateUrl: './daily-expenses.page.html',
  styleUrls: ['./daily-expenses.page.scss']
})
export class DailyExpensesPage implements OnInit {
  public loggedInUser: User = new User();

  expensesForm: FormGroup;
  newExpense: DailyExpenses = {
    expenseDate: new Date(),
    category: 'FUEL',
    expenseDetails: 'Diesel for generators',
    amount: 16500,
    loggedInUser: 'staff@swintec.com',
    approvedBy: 'MD'
  };
  submitAttempt = false;

  constructor(
    private formBuilder: FormBuilder,
    private dlyExpSvc: DailyExpensesService
  ) {
    this.clearEntries();
  }

  ngOnInit() {}

  async save() {
    this.submitAttempt = true;
    if (!this.expensesForm.valid) {
      console.log(`Form is invalid`);
    } else {
      const category: string = this.expensesForm.value.category;
      const expenseDetails: string = this.expensesForm.value.expenseDetails;
      const amount = this.expensesForm.value.amount;
      const loggedInUser = 'staff@swintec.com';
      const approvedBy: string = this.expensesForm.value.approvedBy;

      this.newExpense = {
        expenseDate: new Date(),
        category: category.toUpperCase(),
        expenseDetails: expenseDetails,
        amount: Number(amount).valueOf(),
        loggedInUser: loggedInUser,
        approvedBy: approvedBy
      };
      await this.dlyExpSvc.createNew(this.newExpense);
      this.submitAttempt = false;
      this.clearEntries();
    }
  }

  clearEntries() {
    this.expensesForm = this.formBuilder.group({
      category: ['', Validators.required],
      expenseDetails: ['', Validators.required],
      amount: [0, Validators.required],
      loggedInUser: [''],
      approvedBy: ['GM', Validators.required]
    });
  }
}
