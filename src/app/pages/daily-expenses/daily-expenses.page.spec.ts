import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyExpensesPage } from './daily-expenses.page';

describe('DailyExpensesPage', () => {
  let component: DailyExpensesPage;
  let fixture: ComponentFixture<DailyExpensesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyExpensesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyExpensesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
