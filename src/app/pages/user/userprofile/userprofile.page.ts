import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.page.html',
  styleUrls: ['./userprofile.page.scss']
})
export class UserprofilePage implements OnInit {
  public loggedInUser: User = new User();

  public user: Observable<Object>;
  public id: string;
  public balance = 0.0;
  constructor(private usrSvc: UserService, private route: ActivatedRoute) { }

  ngOnInit() {
    const userid: string = this.route.snapshot.paramMap.get('id');
    this.user = this.usrSvc.getUser(userid);
  }
}
