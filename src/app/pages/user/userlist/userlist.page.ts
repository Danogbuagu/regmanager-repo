import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.page.html',
  styleUrls: ['./userlist.page.scss']
})
export class UserlistPage implements OnInit {
  public loggedInUser: User = new User();

  public users: Observable<Object>;

  constructor(private userSvc: UserService) {}

  ngOnInit() {
    this.users = this.userSvc.getUsers();
  }
}
