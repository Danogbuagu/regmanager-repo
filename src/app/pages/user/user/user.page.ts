import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';
import { AlertUtils } from '../../../helpers/alert-utils';
import { EmailValidator } from '../../../helpers/email-validator';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss']
})
export class UserPage implements OnInit {
  public loggedInUser: User = new User();

  userForm: FormGroup;
  newUser: User = {
    username: 'johnbull',
    email: 'john@bull.com',
    sname: 'Bull',
    fname: 'John',
    onames: 'Shit',
    phoneNo: '00055556666',
    roles: ['user'],
    visits: 0,
    password: 'animalFarm1980',
    active: false
  };

  submitAttempt = false;
  passwordsMatch = false;
  private roles = [''];

  constructor(private formBuilder: FormBuilder,
    private userSvc: UserService,
    private alertUtils: AlertUtils) {
    this.clearEntries();
    const repeat_password = this.userForm.value.repeat_password;
    const password = this.userForm.value.password;
    if (repeat_password === password) {
      this.passwordsMatch = true;
    } else {
      this.passwordsMatch = false;
    }
  }

  ngOnInit() {
    // this.users = this.userSvc.getUsers();
  }

  async register() {
    this.submitAttempt = true;
    if (!this.userForm.valid || !this.passwordsMatch) {
      console.log(`Form is invalid`);
    } else {
      this.userForm.value.sname = this.userForm.value.sname.toUpperCase();

      const sname = this.userForm.value.sname;
      const username = this.userForm.value.username;
      const email = this.userForm.value.email;
      const fname = this.userForm.value.fname;
      const onames = this.userForm.value.onames;
      const phoneNo = this.userForm.value.phoneNo;
      const roles = this.userForm.value.roles;
      const password = this.userForm.value.password;

      this.newUser = {
        username: username.toUpperCase(),
        email: email,
        sname: sname,
        fname: fname,
        onames: onames,
        phoneNo: phoneNo,
        roles: roles,
        visits: 0,
        password: password,
        active: false
      };
      await this.userSvc.createNew(this.newUser);
      this.submitAttempt = false;
      // this.route.navigateByUrl('/userlist');
      await this.alertUtils.presentToast(`New user
      ${this.newUser.sname},
      ${this.newUser.fname} ${this.newUser.onames}
      registered!`, 1000 * 4);
      await this.clearEntries();
    }
  }

  clearEntries() {
    this.userForm = this.formBuilder.group({
      email: [
        '',
        Validators.compose([Validators.required, EmailValidator.isValid])
      ],
      password: [
        '',
        Validators.compose([
          Validators.minLength(6),
          Validators.maxLength(36),
          Validators.required
        ])
      ],
      repeat_password: [
        '',
        Validators.compose([
          Validators.minLength(6),
          Validators.maxLength(36),
          Validators.required
        ])
      ],
      username: [
        '',
        Validators.compose([
          Validators.maxLength(40),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ],
      sname: [
        '',
        Validators.compose([
          Validators.maxLength(40),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ],
      fname: [
        '',
        Validators.compose([
          Validators.maxLength(40),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ],
      onames: [''],
      phoneNo: [''],
      roles: [['user'], Validators.required]
    });
    this.userForm.controls['roles'].setValue(['user']);
  }
}
