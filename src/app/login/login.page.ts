import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertUtils } from '../helpers/alert-utils';
// import { EmailValidator } from '../helpers/email-validator';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  error: any;
  uname = '';
  public loginForm: any;
  usernameChanged = false;
  passwordChanged = false;
  submitAttempt = false;

  loggedIn = false;

  constructor(
    private alrtUtils: AlertUtils,
    public formBuilder: FormBuilder,
    private nav: NavController,
    private userSvc: UserService,
    private router: Router
  ) {
    this.loginForm = formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: [
        '',
        Validators.compose([Validators.minLength(6), Validators.required])
      ]
    });
  }

  ngOnInit() {}

  async loginUser() {
    if (!this.loginForm.valid) {
      console.log(this.loginForm.value);
    } else {
      const username = this.loginForm.value.username;
      const password = this.loginForm.value.password;
      localStorage.clear();
      await this.userSvc
        .signInWithUsernameAndPassword(username, password)
        .subscribe(credential => {
          try {
            const accessTokenFromServer = JSON.stringify(credential);
            // console.log(
            //   `login.page.ts 54: value of accessTokenFromServer, as returned on Login => ${accessTokenFromServer}`
            // );
            this.uname = this.userSvc.usernameFromPayload(
              accessTokenFromServer
            );
            let nameOf = JSON.stringify(this.uname);
            nameOf = JSON.parse(nameOf);
            const usrname = nameOf as string;
               console.log(`Username obtained => ${usrname}`);

            localStorage.setItem(
              'auth_token',
              JSON.stringify(accessTokenFromServer)
            );
            this.loggedIn = true;

            // console.log(`------------ Testing saving and retrieveing token ------------`);
            // const retrieved = localStorage.getItem('auth_token');
            // console.log(`Value retrieved => ${JSON.stringify(retrieved)}`);
            // this.loggedIn = !!localStorage.getItem('auth_token');
            // console.log(`--------------------------------------------------------------`);

            this.router.navigateByUrl(`/home/${usrname}`);

            // this.nav.navigateForward(`/home/${username}`);
          } catch (error) {
            const hdr = `Login Error`;
            const subHdr = 'Errors!';
            const msg = `Unable to log in due to ${error.message}`;

            this.alrtUtils.presentAlert(hdr, subHdr, msg);
            this.router.navigateByUrl('/login');
          }
        });
    }
  }

  // private async useCredential(credential: Object) {
  //   const accessTokenFromServer = JSON.stringify(credential);
  //   this.uname = await this.authSvc.usernameFromPayload(accessTokenFromServer);
  //   console.log(`Extracted Username is ${this.uname}`);
  //   await this.authSvc.saveAuthState(accessTokenFromServer);
  //   this.currentUser = await this.authSvc.getUserByUsername(this.uname);
  //   const hdr = `Login Success`;
  //   const subHdr = 'Success!';
  //   const msg = `Logged in as ${this.currentUser.sname},     ${
  //     this.currentUser.fname
  //   } ${this.currentUser.onames},
  //   Using Access token returned from REST server API ${accessTokenFromServer}`;
  //   this.alrtUtils.presentAlert(hdr, subHdr, msg);
  //   this.router.navigateByUrl('/home');
  // }
}
