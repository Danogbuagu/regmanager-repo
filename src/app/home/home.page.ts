import { Component, OnInit, NgZone } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../models/user';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {
  public user: User = new User();
  username = '';
  loggedIn = false;
  loggedOut = false;
  currentUser: string;
  constructor(
    private userSvc: UserService,
    private route: ActivatedRoute, private zone: NgZone,
    private router: Router
  ) {
    if (this.loggedIn) {
      this.loggedOut = false;
    } else {
      this.loggedOut = true;
    }
  }

  ngOnInit() {

    this.username = this.route.snapshot.paramMap.get('id');

    let arr: User[];

    const allUsers: Observable<Object> = this.userSvc.getUsers();
    allUsers.subscribe(usr => {
      console.log(`All users: ${JSON.stringify(usr)}`);
      this.zone.run(() => {
        arr = usr as User[];
        this.user = arr.find(_ => _.username === this.username);
      });
      if (this.user) {
        this.loggedIn = true;
        this.currentUser =
          this.user.sname + ', ' + this.user.fname + ' ' + this.user.onames;
      }
    });
  }

  logout() {
    this.userSvc.signOut();
    this.router.navigateByUrl('/login');
  }
}
